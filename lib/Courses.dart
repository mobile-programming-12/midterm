import 'package:flutter/material.dart';

const List<String> list = <String>['คณะแพทยศาสตร์', 'คณะเภสัชศาสตร์', 'คณะพยาบาลศาสตร์','คณะมนุษยศาสตร์และสังคมศาสตร์','คณะภูมิสารสนเทศศาสตร์','คณะบริหารธุรกิจ','คณะรัฐศาสตร์และนิติศาสตร์','คณะวิทยาศาสตร์','คณะวิทยาการสารสนเทศ','คณะวิศวกรรมศาสตร์','คณะสาธารณสุขศาสตร์','คณะศิลปกรรมศาสตร์','คณะดนตรีและการแสดง','คณะศึกษาศาสตร์','คณะโลจิสติกส์','คณะวิทยาศาสตร์การกีฬา','วิทยาลัยนานาชาติ','คณะสหเวชศาสตร์','คณะวิทยาศาตร์และศิลปศาสตร์','คณะเทคโนโลยีทางทะเล','คณะอัญมณี','คณะวิทยาศาตร์และสังคมศาสตร์','คณะเทคโนโลยีการเกษตร'];

class DropdownButtonExample extends StatefulWidget {
  const DropdownButtonExample({super.key});

  @override
  State<DropdownButtonExample> createState() => _DropdownButtonExampleState();
}

class _DropdownButtonExampleState extends State<DropdownButtonExample> {
  String dropdownValue = list.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 16,
      style: const TextStyle(color: Colors.grey,fontSize: 17.50),
      underline: Container(
        height: 2,
        color: Colors.grey,
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: list.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
