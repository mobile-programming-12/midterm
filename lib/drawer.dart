import 'package:flutter/material.dart';
import 'Calender.dart';
import 'teacher_schedule.dart';
import 'Schedule.dart';
import 'Courses2.dart';
import 'Registration.dart';
import 'Login.dart';
import 'Student_History.dart';
import 'main.dart';

class Nevegationdrawer extends StatelessWidget {
  const Nevegationdrawer({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.yellow),
            accountName: Text(
              "Sakda Onprom",
              style: TextStyle(color: Colors.black),
            ),
            accountEmail: Text(
              "63160222@go.buu.ac.th",
              style: TextStyle(color: Colors.black),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                'assets/images/imgme.jpg',
              ),
            ),
          ),
          ListTile(
            leading: Icon(
              Icons.home,
            ),
            title: const Text(
              'หน้าหลัก',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new MyAppREG()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.calendar_view_month_outlined,
            ),
            title: const Text(
              'ปฏิทินการศึกษา',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new CalendarTable1()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.menu_book_outlined,
            ),
            title: const Text(
              'หลักสูตรที่เปิดสอน',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (context) => new Courses()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.calendar_month_outlined,
            ),
            title: const Text(
              'ตารางเรียนนิสิต',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ScheduleTable()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.calendar_month,
            ),
            title: const Text(
              'ตารางสอนอาจารย์',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new TeacherTeachingSchedule()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.app_registration,
            ),
            title: const Text(
              'ลงทะเบียน',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('ลงทะเบียนแล้ว'),
                  content: const Text('นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: const Text('ตกลง',style: TextStyle(color: Colors.green),),
                    ),
                  ],
                ),
              );
            },
          ),
          ListTile(
            leading: Icon(
              Icons.app_registration_sharp,
            ),
            title: const Text(
              'ผลการลงทะเบียน',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new RegistrationsTable()));
            },
          ),
          
          ListTile(
            leading: Icon(
              Icons.account_circle,
            ),
            title: const Text(
              'ประวัตินิสิต',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new HistorysTable()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.logout,
            ),
            title: const Text(
              'ออกจากระบบ',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new LoginDemo()));
            },
          ),
        ],
      ),
    );
  }
}
