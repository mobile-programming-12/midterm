import 'package:flutter/material.dart';

class CalendarCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const CalendarCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}

class CalendarTable extends StatelessWidget {
  const CalendarTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth>{2: FixedColumnWidth(100)},
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.yellow.shade300,
            ),
            children: const <Widget>[
              CalendarCell(title: 'รายการ', isHeader: true),
              CalendarCell(title: 'วันเริ่มต้น', isHeader: true),
              CalendarCell(title: 'วันที่สิ้นสุด', isHeader: true),
            ],
          ),
          ...CalendarList.getCalendarList().map((calender) {
            return TableRow(children: [
              CalendarCell(title: calender.name),
              CalendarCell(title: calender.first),
              CalendarCell(title: calender.end),
            ]);
          }),
        ],
      ),
    );
  }
}

class CalendarList {
  final String name;
  final String first;
  final String end;

  CalendarList(this.name, this.first, this.end);

  static List<CalendarList> getCalendarList() {
    return [
      CalendarList('ช่วงจองรายวิชาลงทะเบียนให้กับนิสิตโดยเจ้าหน้าที่',
          '1 พ.ย. 2565 8:30 น.', '18 พ.ย. 2565 23:59 น.'),
      CalendarList('ลงทะเบียนปกติ ( on-line )', '11 พ.ย. 2565 8:30 น..',
          '18 พ.ย. 2565 23:59 น.'),
      CalendarList(
          'วันเปิดภาคการศึกษา', '19 พ.ย. 2565 0:00 น.', '19 พ.ย. 2565 0:00 น.'),
      CalendarList('ช่วงวันทำการเพิ่ม-ลดรายวิชา', '21 พ.ย. 2565 8:30 น.',
          '28 พ.ย. 2565 23:59 น.'),
      CalendarList(
          'ช่วงชำระค่าเทอม', '15 ธ.ค. 2565 0:00 น.', '22 ธ.ค. 2565 22:30 น.'),
      CalendarList(
          'ช่วงวันสอบกลางภาค', '14 ม.ค. 2566 0:00 น.', '22 ม.ค. 2566 0:00 น.'),
      CalendarList(
          'ช่วงวันสอบปลายภาค', '25 มี.ค. 2566 0:00 น.', '2 เม.ย. 2566 0:00 น.'),
      CalendarList(
          'วันปิดภาคการศึกษา', '3 เม.ย. 2566 0:00 น.', '3 เม.ย. 2566 0:00 น.'),
    ];
  }
}

class CalendarTable1 extends StatelessWidget {
  const CalendarTable1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('assets/images/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.language,
                  color: Colors.black,
                ),
                onPressed: () {}),
          ],
        ),
        body: ListView(children: const <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ปฏิทินการศึกษา',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ปีการศึกษา 2565/2',
              style: TextStyle(fontSize: 17),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: CalendarTable(),
          ),
        ]),
      ),
    );
  }
}
