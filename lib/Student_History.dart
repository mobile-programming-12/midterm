import 'package:flutter/material.dart';

class HistoryCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const HistoryCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}

class HistoryTable extends StatelessWidget {
  const HistoryTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth>{2: FixedColumnWidth(100)},
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.yellow.shade300,
            ),
            children: const <Widget>[
              HistoryCell(title: 'ข้อมูลด้านการศึกษา', isHeader: true),
              HistoryCell(title: '', isHeader: true),
            ],
          ),
          ...History.getHistoryList().map((registration) {
            return TableRow(children: [
              HistoryCell(title: registration.details),
              HistoryCell(title: registration.data),
            ]);
          }),
        ],
      ),
    );
  }
}

class History {
  final String details;
  final String data;

  History(this.details, this.data);

  static List<History> getHistoryList() {
    return [
      History('รหัสประจำตัว:', '63160222'),
      History('เลขที่บัตรประชาชน:', '1431911119394'),
      History('ชื่อ:', 'นายศักดา อ่อนพรม'),
      History('ชื่ออังกฤษ:', 'MR. SAKDA ONPROM'),
      History('คณะ:', 'คณะวิทยาการสารสนเทศ'),
      History('ระดับการศึกษา:', 'ปริญญาตรี '),
      History('หลักสูตร:', '2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ'),
      History('ชื่อปริญญา:', 'วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์)'),
      History('สถานภาพ:', 'โสด'),
      History('อ. ที่ปรึกษา:', 'อาจารย์ภูสิต กุลเกษม\nผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน '),
    ];
  }
}

class HistorysTable extends StatelessWidget {
  const HistorysTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('assets/images/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: const [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.language,
                  color: Colors.black,
                ),
                onPressed: () {}),
          ],
        ),
        body: ListView(children: const <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ระเบียนประวัติ',
              style: TextStyle(fontSize: 25),
            ),
          ),
          CircleAvatar(
            radius: 125,
            backgroundImage: AssetImage('assets/images/imgme.jpg'),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: HistoryTable(),
          ),
        ]

            ),
      ),
    );
  }
}
