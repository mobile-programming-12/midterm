import 'package:flutter/material.dart';

class RegistrationCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const RegistrationCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}

class RegistrationTable extends StatelessWidget {
  const RegistrationTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth>{2: FixedColumnWidth(100)},
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.yellow.shade300,
            ),
            children: const <Widget>[
              RegistrationCell(title: 'รหัสวิชา', isHeader: true),
              RegistrationCell(title: 'ชื่อรายวิชา', isHeader: true),
              RegistrationCell(title: 'แบบการศึกษา', isHeader: true),
              RegistrationCell(title: 'หน่วยกิต', isHeader: true),
              RegistrationCell(title: 'กลุ่ม', isHeader: true),
            ],
          ),
          ...Registration.getCalendarList().map((registration) {
            return TableRow(children: [
              RegistrationCell(title: registration.code),
              RegistrationCell(title: registration.name),
              RegistrationCell(title: registration.type),
              RegistrationCell(title: registration.credit.toString(),),
              RegistrationCell(title: registration.group.toString(),),
            ]);
          }),
        ],
      ),
    );
  }
}

class Registration {
  final String code;
  final String name;
  final String type;
  final int credit;
  final int group;

  Registration(this.code, this.name, this.type, this.credit, this.group);

  static List<Registration> getCalendarList() {
    return [
      Registration(
          '88624359', 'Web Programming\nการเขียนโปรแกรมบนเว็บ', 'GD', 3,1),
          Registration(
          '88624459', 'Object-Oriented Analysis and Design\nการวิเคราะห์และออกแบบเชิงวัตถุ', 'GD', 3,1),
          Registration(
          '88624559', 'Software Testing\nการทดสอบซอฟต์แวร์', 'GD', 3,1),
          Registration(
          '88634259', 'Multimedia Programming for Multiplatforms\nการโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม', 'GD', 3,1),
          Registration(
          '88634459', 'Mobile Application Development I\nการพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1', 'GD', 3,1),
          Registration(
          '88646259', 'Introduction to Natural Language Processing\nการประมวลผลภาษาธรรมชาติเบื้องต้น', 'GD', 3,1),
          
    ];
  }
}

class RegistrationsTable extends StatelessWidget {
  const RegistrationsTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('assets/images/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: const [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.language,
                  color: Colors.black,
                ),
                onPressed: () {}),
          ],
        ),
        body: ListView(children: const <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ผลการลงทะเบียน',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ปีการศึกษา 2565/2',
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: RegistrationTable(),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'รวมหน่วยกิตทั้งหมด 18 หน่วยกิต',
              style: TextStyle(fontSize: 25),textAlign: TextAlign.center,
            ),
          ),
        ]

            // child: Padding(
            //   padding: EdgeInsets.all(8.0),
            //   child: CalendarTable(),
            // ),
            ),
      ),
    );
  }
}
