import 'package:flutter/material.dart';

class ScheduleCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const ScheduleCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}

class CalendarTable extends StatelessWidget {
  const CalendarTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500.0),
      child: Table(
        columnWidths: const <int, TableColumnWidth>{2: FixedColumnWidth(100)},
        defaultColumnWidth: const FlexColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: <TableRow>[
          TableRow(
            decoration: BoxDecoration(
              color: Colors.yellow.shade300,
            ),
            children: const <Widget>[
              ScheduleCell(title: 'วัน', isHeader: true),
              ScheduleCell(title: 'รายวิชา', isHeader: true),
              ScheduleCell(title: 'เวลา', isHeader: true),
              ScheduleCell(title: 'ห้องเรียน', isHeader: true),
            ],
          ),
          ...Schedule.getCalendarList().map((schedule) {
            return TableRow(children: [
              ScheduleCell(title: schedule.day),
              ScheduleCell(title: schedule.name),
              ScheduleCell(title: schedule.time),
              ScheduleCell(title: schedule.classroom),
            ]);
          }),
        ],
      ),
    );
  }
}

class Schedule {
  final String day;
  final String name;
  final String time;
  final String classroom;

  Schedule(this.day, this.name, this.time, this.classroom);

  static List<Schedule> getCalendarList() {
    return [
      Schedule(
          'จันทร์', 'Software Testing', '10:00 น - 12:00 น.', 'IF-4M210'),
      Schedule('', 'Object-Oriented Analysis and Design', '13:00 น - 15:00 น.',
          'IF-3M210'),
      Schedule('', 'Web Programming', '17:00 น - 19:00 น.', 'IF-3M210'),
      Schedule('อังคาร', 'Multimedia Programming for Multiplatforms', '10:00 น - 12:00 น.', 'IF-4C01'),
      Schedule('', 'Object-Oriented Analysis and Design', '13:00 น - 15:00 น.', 'IF-4C01'),
      Schedule('', 'Software Testing', '15:00 น - 17:00 น.', 'IF-3C03'),
      Schedule('พุธ', 'Mobile Application Development I', '10:00 น - 12:00 น.', 'IF-4C01'),
      Schedule('', 'Introduction to Natural Language Processing', '13:00 น - 16:00 น.', 'IF-6T01'),
      Schedule('', 'Web Programming', '17:00 น - 19:00 น.', 'IF-3C01'),
      Schedule('ศุกร์', 'Multimedia Programming for Multiplatforms', '13:00 น - 15:00 น.', 'IF-4C01'),
      Schedule('', 'Mobile Application Development I', '15:00 น - 17:00 น.', 'IF-3C01'),
    ];
  }
}

class ScheduleTable extends StatelessWidget {
  const ScheduleTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('assets/images/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.language,
                  color: Colors.black,
                ),
                onPressed: () {}),
          ],
        ),
        body: ListView(children: const <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ตารางเรียน/สอบ',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: CalendarTable(),
          ),
        ]

         
            ),
      ),
    );
  }
}
