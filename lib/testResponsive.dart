import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:midterm/main.dart';
import 'Login.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        theme: ThemeData(
          colorSchemeSeed: Colors.black, useMaterial3: true),
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        home: Scaffold(
          backgroundColor: Colors.white,
          body: LoginDemo(),
        ),
      ),
    );
  }
}
