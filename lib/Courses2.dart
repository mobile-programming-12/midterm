import 'package:flutter/material.dart';
import 'Courses.dart';
class Courses extends StatelessWidget {
  const Courses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('assets/images/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: const [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.language,
                  color: Colors.black,
                ),
                onPressed: () {}),
          ],
        ),
        body: ListView(
          children: const <Widget>[
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'หลักสูตร',
                style: TextStyle(fontSize: 25),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'คณะ :',
                style: TextStyle(fontSize: 20),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(1.0),
              child: DropdownButtonExample(),
            ),
            
            
          ],
        ),
      ),
    );
  }
}
