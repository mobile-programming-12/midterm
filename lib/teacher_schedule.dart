import 'package:flutter/material.dart';
class TeacherTeachingSchedule extends StatelessWidget {
  const TeacherTeachingSchedule({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('assets/images/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.language,
                  color: Colors.black,
                ),
                onPressed: () {}),
          ],
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'ตารางสอนอาจารย์',
                style: TextStyle(fontSize: 25),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'โปรดระบุชื่อท่านอาจารย์',
                style: TextStyle(fontSize: 17),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: '',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: ElevatedButton(
                child: Text(
                  "ค้นหา",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.yellow,
                  elevation: 20,
                ),
                onPressed: () {},
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'คำแนะนำ \n  1. ถ้าต้องการค้นหาอาจารย์ที่มีชื่อขึ้นต้นด้วย สม ให้ป้อน สม* \n 2. ถ้าต้องการค้นหาอาจารย์ที่มีชื่อลงท้ายด้วย ชาย ให้ป้อน *ชาย \n 3. ระบุจำนวนผลลัพธ์ของรายชื่อที่ต้องการ \n 4. กดปุ่ม "ค้นหา" เพื่อเริ่มทำการค้นหาตามเงื่อนไข',
                style: TextStyle(fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
